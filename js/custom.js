/* ========================================================================= */
/*	Preloader
 /* ========================================================================= */

jQuery(window).load(function () {
    $("#preloader").fadeOut("slow");
});


$(document).ready(function () {

    /* ========================================================================= */
    /*	Menu item highlighting
     /* ========================================================================= */

    jQuery('#nav').singlePageNav({
        offset: jQuery('#nav').outerHeight(),
        filter: ':not(.external)',
        speed: 1000,
        currentClass: 'current',
        easing: 'easeInOutExpo',
        updateHash: true
    });

    function scrollNav() {
        if ($(window).scrollTop() > 400) {
            $("#navigation").addClass('scroll');
        } else {
            $("#navigation").removeClass('scroll');
        }
    }

    scrollNav();
    $(window).scroll(function () {
        scrollNav();
    });

    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("in");
        if (_opened === true && !clickover.hasClass("navbar-toggle")) {
            $("button.navbar-toggle").click();
        }
    });

    $(".navbar-toggle").on('click', function(){
        $(this).toggleClass('collapsed');
    });

    /* ========================================================================= */
    /*	Fix Slider Height
     /* ========================================================================= */

    var slideHeight = $(window).height();

    $('.fullwidth-carousel, .fullwidth-carousel .item-slide').css('height', slideHeight);

    $(window).resize(function () {
        'use strict',
            $('.fullwidth-carousel, .fullwidth-carousel .item-slide').css('height', slideHeight);
    });

    $(".fullwidth-carousel").slick({
      dots: true,
      arrows: false,
      infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
      speed: 500,
      autoplaySpeed: 2000,
       cssEase: 'ease-out'
    });

    $(".fullwidth-carousel").on('beforeChange', function(event, slick, currentSlide, nextSlide){
    if(currentSlide != nextSlide) {
          var wow_slide = new WOW({
                  boxClass: 'wow-slide',
                  animateClass: 'animated',
                  offset: 120,
                  mobile: true,
                  live: true
              }
          );
          wow_slide.init();
      }
    });

    /* ========================================================================= */
    /*	Parallax
     /* ========================================================================= */

    $('#facts').parallax("50%", 0.3);


    /* ========================================================================= */
    /*	Animation
     /* ========================================================================= */

    if($(window).width() < 768) {
        $('.no-delay-mobile').attr('data-wow-delay', '300ms');
    }

    /* ========================================================================= */
    /*	Back to Top
     /* ========================================================================= */

    var lastScrollTop = 0;
    $(window).scroll(function () {
        var st = $(this).scrollTop();
        if (st > lastScrollTop || st < 100) {
            $("#back-top").removeClass('show-icon').addClass('hide-icon')
        } else {
            $("#back-top").removeClass('hide-icon').addClass('show-icon')
        }
        lastScrollTop = st;
    });
    $("#back-top").click(function () {
        $("html, body").stop().animate({
            scrollTop: 0
        }, 1500, "easeInOutExpo")
    });

});